#!/bin/bash

#BSUB -J acc_roussp01a
#BSUB -P acc_roussp01a
#BSUB -q premium
#BSUB -n 4
#BSUB -R span[ptile=4]
#BSUB -R rusage[mem=12000]
#BSUB -W 27:00
#BSUB -m manda
#BSUB -o %J.stdout
#BSUB -eo %J.stderr
#BSUB -L /bin/bash

Rscript ./create_model.R DLPFC ../../data/intermediate/expression_phenotypes/DLPFC.expr1.RDS ../../data/intermediate/genotypes/CMC.snps.chr22.txt ../../data/intermediate/annotations/gene_annotation/gencode.v12.genes.parsed.RDS ../../data/intermediate/annotations/snp_annotation/CMC.annot.chr22.RDS /sc/orga/projects/roussp01a/Wen/DLPF_annoprior/DLPFCBRN_Anno_prior_rsid.chr22.RDS 10 0.5 ../../data/intermediate/model_by_chr/ 22 1KG 1e6 > chr22.out
